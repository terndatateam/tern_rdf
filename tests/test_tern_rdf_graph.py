def test_instantiating_ternrdf_graph():
    from tern_rdf import TernRdf
    from tern_rdf.namespace_bindings import TERN
    from rdflib import URIRef, RDF

    g = TernRdf.Graph()
    g.add((URIRef("https://w3id.org/tern/resources/test"), RDF.type, TERN.Site))
    assert len(g) == 1
    value = g.serialize(format="turtle")
    assert "@prefix tern: <https://w3id.org/tern/ontologies/tern/> ." in value


def test_instantiating_ternrdf_graph_ausplots():
    from tern_rdf import TernRdf, AUSPLOTS_BINDINGS
    from tern_rdf.namespace_bindings import TERN
    from rdflib import URIRef, RDF

    g = TernRdf.Graph(
        [
            AUSPLOTS_BINDINGS,
        ]
    )
    g.add((URIRef("http://linked.data.gov.au/dataset/ausplots/test"), RDF.type, TERN.Site))
    assert len(g) == 1
    value = g.serialize(format="turtle")
    assert "@prefix ausplots: <http://linked.data.gov.au/dataset/ausplots/> ." in value


def test_instantiating_ternrdf_graph_corveg():
    from tern_rdf import TernRdf, CORVEG_BINDINGS
    from tern_rdf.namespace_bindings import TERN
    from rdflib import URIRef, RDF

    g = TernRdf.Graph(
        [
            CORVEG_BINDINGS,
        ]
    )
    g.add((URIRef("http://linked.data.gov.au/dataset/corveg/test"), RDF.type, TERN.Site))
    assert len(g) == 1
    value = g.serialize(format="turtle")
    assert "@prefix corveg: <http://linked.data.gov.au/dataset/corveg/> ." in value
