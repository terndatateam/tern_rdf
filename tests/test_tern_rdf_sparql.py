def test_sparql():
    from tern_rdf.sparql import sparql
    query = """
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

        SELECT ?label ?uri ?altLabel ?altLabel2 ?type ?exactMatch ?source ?description
        WHERE {
            SERVICE <https://vocabs.ardc.edu.au/repository/api/sparql/ardc-curated_gcmd-platforms_9-1-5-2020-02-04> {
                # NOAA satellites
                {
                    ?c a skos:Concept ;
                       skos:prefLabel ?label .
                    OPTIONAL { ?c skos:altLabel ?altLabel }
                    OPTIONAL { ?c skos:definition ?description }

                    FILTER REGEX(?label, "NOAA-[0-9]*")

                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://w3id.org/tern/resources/") AS ?uri)
                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://gcmd.earthdata.nasa.gov/kms/concept/") AS ?gcmd_uri)
                    BIND(tern:EarthObservationSatellite AS ?type)
                    BIND(?gcmd_uri AS ?exactMatch)
                    BIND(?gcmd_uri AS ?source)
                }
                # ALOS satellites
                UNION {
                    ?c a skos:Concept ;
                       skos:prefLabel ?label .
                    OPTIONAL { ?c skos:altLabel ?altLabel }
                    OPTIONAL { ?c skos:definition ?description }

                    FILTER REGEX(?label, "ALOS[-]?[0-9]*")

                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://w3id.org/tern/resources/") AS ?uri)
                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://gcmd.earthdata.nasa.gov/kms/concept/") AS ?gcmd_uri)
                    BIND(tern:EarthObservationSatellite AS ?type)
                    BIND(?gcmd_uri AS ?exactMatch)
                    BIND(?gcmd_uri AS ?source)
                }
                # ICESat satellites
                UNION {
                    ?c a skos:Concept ;
                       skos:prefLabel ?label .
                    OPTIONAL { ?c skos:altLabel ?altLabel }
                    OPTIONAL { ?c skos:definition ?description }

                    FILTER REGEX(?label, "ICESat[-]?[0-9]*")

                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://w3id.org/tern/resources/") AS ?uri)
                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://gcmd.earthdata.nasa.gov/kms/concept/") AS ?gcmd_uri)
                    BIND(tern:EarthObservationSatellite AS ?type)
                    BIND(?gcmd_uri AS ?exactMatch)
                    BIND(?gcmd_uri AS ?source)
                }
                # LANDSAT satellites
                UNION {
                    ?c a skos:Concept ;
                       skos:prefLabel ?label .
        #            OPTIONAL {
        #                SELECT ?c ?altLabel WHERE {
        #                    ?c skos:altLabel ?altLabel                 
        #                } LIMIT 1
        #            }
                    OPTIONAL { ?c skos:altLabel ?altLabel }
                    OPTIONAL { ?c skos:definition ?description }

                    FILTER REGEX(?label, "LANDSAT-[0-9]*")

                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://w3id.org/tern/resources/") AS ?uri)
                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://gcmd.earthdata.nasa.gov/kms/concept/") AS ?gcmd_uri)
                    BIND(tern:EarthObservationSatellite AS ?type)
                    BIND(?gcmd_uri AS ?exactMatch)
                    BIND(?gcmd_uri AS ?source)
                }
                # SPOT satellites
                UNION {
                    ?c a skos:Concept ;
                       skos:prefLabel ?label .
        #            OPTIONAL {
        #                SELECT ?c ?altLabel WHERE {
        #                    ?c skos:altLabel ?altLabel                 
        #                } LIMIT 1
        #            }
                    OPTIONAL { ?c skos:altLabel ?altLabel }
                    OPTIONAL { ?c skos:definition ?description }

                    FILTER REGEX(?label, "SPOT-[0-9]*")

                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://w3id.org/tern/resources/") AS ?uri)
                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://gcmd.earthdata.nasa.gov/kms/concept/") AS ?gcmd_uri)
                    BIND(tern:EarthObservationSatellite AS ?type)
                    BIND(?gcmd_uri AS ?exactMatch)
                    BIND(?gcmd_uri AS ?source)
                }
                # SENTINEL satellites
                UNION {
                    ?c a skos:Concept ;
                       skos:prefLabel ?label .
        #            OPTIONAL {
        #                SELECT ?c ?altLabel WHERE {
        #                    ?c skos:altLabel ?altLabel                 
        #                } LIMIT 1
        #            }
                    OPTIONAL { ?c skos:altLabel ?altLabel }
                    OPTIONAL { ?c skos:definition ?description }

                    FILTER REGEX(?label, "SENTINEL-[0-9][A-Z]")

                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://w3id.org/tern/resources/") AS ?uri)
                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://gcmd.earthdata.nasa.gov/kms/concept/") AS ?gcmd_uri)
                    BIND(tern:EarthObservationSatellite AS ?type)
                    BIND(?gcmd_uri AS ?exactMatch)
                    BIND(?gcmd_uri AS ?source)
                }
                # Terra
                UNION {
                    ?c a skos:Concept ;
                       skos:prefLabel ?label .
        #            OPTIONAL {
        #                SELECT ?c ?altLabel WHERE {
        #                    ?c skos:altLabel ?altLabel                 
        #                } LIMIT 1
        #            }
                    OPTIONAL { ?c skos:altLabel ?altLabel }
                    OPTIONAL { ?c skos:definition ?description }

                    FILTER REGEX(?label, "^Terra$")

                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://w3id.org/tern/resources/") AS ?uri)
                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://gcmd.earthdata.nasa.gov/kms/concept/") AS ?gcmd_uri)
                    BIND(tern:EarthObservationSatellite AS ?type)
                    BIND(?gcmd_uri AS ?exactMatch)
                    BIND(?gcmd_uri AS ?source)
                }
                # Aqua
                UNION {
                    ?c a skos:Concept ;
                       skos:prefLabel ?label .
        #            OPTIONAL {
        #                SELECT ?c ?altLabel WHERE {
        #                    ?c skos:altLabel ?altLabel                 
        #                } LIMIT 1
        #            }
                    OPTIONAL { ?c skos:altLabel ?altLabel }
                    OPTIONAL { ?c skos:definition ?description }

                    FILTER REGEX(?label, "^Aqua$")

                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://w3id.org/tern/resources/") AS ?uri)
                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://gcmd.earthdata.nasa.gov/kms/concept/") AS ?gcmd_uri)
                    BIND(tern:EarthObservationSatellite AS ?type)
                    BIND(?gcmd_uri AS ?exactMatch)
                    BIND(?gcmd_uri AS ?source)
                }
                # Himawari satellite
                UNION {
                    ?c a skos:Concept ;
                       skos:prefLabel ?label .
        #            OPTIONAL {
        #                SELECT ?c ?altLabel WHERE {
        #                    ?c skos:altLabel ?altLabel                 
        #                } LIMIT 1
        #            }
                    OPTIONAL { ?c skos:altLabel ?altLabel }
                    OPTIONAL { ?c skos:definition ?description }

                    FILTER REGEX(?label, "Himawari-[0-9]")

                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://w3id.org/tern/resources/") AS ?uri)
                    BIND(REPLACE(STR(?c), "https://gcmdservices.gsfc.nasa.gov/kms/concept/", "https://gcmd.earthdata.nasa.gov/kms/concept/") AS ?gcmd_uri)
                    BIND(tern:EarthObservationSatellite AS ?type)
                    BIND(?gcmd_uri AS ?exactMatch)
                    BIND(?gcmd_uri AS ?source)
                }
            }
        } 
        ORDER BY ?label
    """
    data = sparql('https://vocabs.ardc.edu.au/repository/api/sparql/ardc-curated_gcmd-platforms_9-1-5-2020-02-04', query)
    assert data
