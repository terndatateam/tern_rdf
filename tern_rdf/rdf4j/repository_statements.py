"""Manage statements in an RDF4J repository.

The statements for a specific repository with ID <ID> are available at:
<RDF4J_URL>/repositories/<ID>/statements

See https://rdf4j.org/documentation/reference/rest-api/#repository-statements
for more information.
"""

import logging
from typing import Union

from tern_rdf.utils import create_session


def put(
    url: str,
    context: str,
    content_type: str,
    content: Union[str, bytes],
    auth_username: str,
    auth_password: str
):
    """Add data to a specific context in the repository, replacing any data that
    is currently in this context.

    :param url: URL of the RDF4j repository.
    :param context: Named graph
    :param content_type: The HTTP header content type to indicate the media type
        of the resource.
    :param content: The RDF content as a string.
    :param auth_username: The username used to authenticate.
    :param auth_password: The password used to authenticate.
    """

    logger = logging.getLogger(__name__)

    params = {'context': f'<{context}>'}
    headers = {'content-type': content_type}
    auth = (auth_username, auth_password)

    logger.info(f'Sending PUT request to {url}.')

    http = create_session()

    # Request timeout set to 600 seconds (10 min).
    r = http.put(url, params=params, data=content, headers=headers, auth=auth, timeout=600)

    logger.info(f'Request completed with status code {r.status_code}.')
    assert r.status_code == 204, r.content.decode('utf-8')


def post(
    url: str,
    context: str,
    content_type: str,
    content: Union[str, bytes],
    auth_username: str,
    auth_password: str,
):
    """Add data to a specific context in the repository to data that
    is currently in this context.

    :param url: URL of the RDF4j repository.
    :param context: Named graph
    :param content_type: The HTTP header content type to indicate the media type
        of the resource.
    :param content: The RDF content as a string.
    :param auth_username: The username used to authenticate.
    :param auth_password: The password used to authenticate.
    """

    logger = logging.getLogger(__name__)

    params = {"context": f"<{context}>"}
    headers = {"content-type": content_type}
    auth = (auth_username, auth_password)

    logger.info(f"Sending POST request to {url}.")

    http = create_session()

    # Request timeout set to 600 seconds (10 min).
    r = http.post(url, params=params, data=content, headers=headers, auth=auth, timeout=600)

    logger.info(f"Request completed with status code {r.status_code}.")
    assert r.status_code == 204, r.content.decode("utf-8")
