from rdflib import OWL, Dataset, Graph, Namespace
from rdflib.namespace import DC, DCTERMS, RDFS, SKOS

AUSPLOTS_BINDINGS = "AUSPLOTS"
AUSPLOTS_FOREST_BINDINGS = "AUSPLOTS-FOREST"
CORVEG_BINDINGS = "CORVEG"
WET_TROPICS_VERT_BINDINGS = "WILLIAMS_WET_TROPICS_VERTEBRATES_DATASET"
TERN_ECOSYSTEM_PROC_BINDINGS = "TERN_ECOSYSTEM_PROCESSES"
BDBSA_BINDINGS = "BDBSA"
THREE_PARKS_SAVANNA_BINDINGS = "THREE_PARKS_SAVANNA"
NSW_FMIP_BINDINGS = "NSW_FMIP"

# TERN-specific namespaces.
ADMS = Namespace("http://www.w3.org/ns/adms#")
BIODIVERSITY = Namespace("http://linked.data.gov.au/def/biodiversity/")
BIOREGION = Namespace("http://linked.data.gov.au/dataset/bioregion/")
DATA = Namespace("http://linked.data.gov.au/def/datatype/")
DWCT = Namespace("http://rs.tdwg.org/dwc/terms/")
EPSG = Namespace("http://www.opengis.net/def/crs/epsg/0/")
GEOSPARQL = Namespace("http://www.opengis.net/ont/geosparql#")
SF = Namespace("http://www.opengis.net/ont/sf")
GEOSPARQL_EXT = Namespace("http://linked.data.gov.au/def/geox#")
LDP = Namespace("http://www.w3.org/ns/ldp#")
LOCN = Namespace("http://www.w3.org/ns/locn#")
PROV = Namespace("http://www.w3.org/ns/prov#")
PLOT = Namespace("http://linked.data.gov.au/def/plot/")
PLOTX = Namespace("http://linked.data.gov.au/def/plot/x/")
REG = Namespace("http://purl.org/linked-data/registry/")
SOIL_TEXTURE = Namespace("http://registry.it.csiro.au/sandbox/soil-data-ie/def/voc/texture/")
SOSA = Namespace("http://www.w3.org/ns/sosa/")
SSN_EXT = Namespace("http://www.w3.org/ns/ssn/ext/")
SSN = Namespace("http://www.w3.org/ns/ssn/")
TIME = Namespace("http://www.w3.org/2006/time#")
UI = Namespace("http://purl.org/linked-data/registry-ui#")
UNIT = Namespace("http://qudt.org/vocab/unit/")
VOID = Namespace("http://rdfs.org/ns/void#")
W3CGEO = Namespace("http://www.w3.org/2003/01/geo/wgs84_pos#")
DCAT = Namespace("http://www.w3.org/ns/dcat#")
SCHEMA = Namespace("http://schema.org/")
DCTYPE = Namespace("http://purl.org/dc/dcmitype/")

# TERN namespaces
TERN_SSN = Namespace("https://w3id.org/tern/ontologies/ssn/")
TERN_LOC = Namespace("https://w3id.org/tern/ontologies/loc/")
TERN_CV = Namespace("http://linked.data.gov.au/def/tern-cv/")
TERN_SD = Namespace("https://w3id.org/tern/ontologies/sd/")
TERN = Namespace("https://w3id.org/tern/ontologies/tern/")

# Corveg Namespaces
CORVEG = Namespace("http://linked.data.gov.au/def/corveg/")
CORVEG_CV = Namespace("http://linked.data.gov.au/def/corveg-cv/")
CORVEG_DATASET = Namespace("http://linked.data.gov.au/dataset/corveg/")

# Ausplots Namespaces
AUSPLOTS = Namespace("http://linked.data.gov.au/def/ausplots/")
AUSPLOTS_DATASET = Namespace("http://linked.data.gov.au/dataset/ausplots/")
AUSPLOTS_CV = Namespace("http://linked.data.gov.au/def/ausplots-cv/")

# Ausplots Forests Namespaces
AUSPLOTS_FOREST = Namespace("http://linked.data.gov.au/def/ausplots-forest/")
AUSPLOTS_FOREST_DATASET = Namespace("http://linked.data.gov.au/dataset/ausplots-forest/")
AUSPLOTS_FOREST_LARGE_TREE_DATASET = Namespace(
    "http://linked.data.gov.au/dataset/ausplots-forest/dataset-large-tree-survey/"
)
AUSPLOTS_FOREST_FUEL_LOAD_DATASET = Namespace(
    "http://linked.data.gov.au/dataset/ausplots-forest/dataset-fuel-load"
)

WET_TROPICS_VERT = Namespace("http://linked.data.gov.au/def/wet-tropics-vertebrate/")
WET_TROPICS_VERT_DATASET = Namespace("http://linked.data.gov.au/dataset/wet-tropics-vertebrate/")

TERN_ECOSYSTEM_PROC = Namespace("http://linked.data.gov.au/def/tern-ecosystem-processes/")
TERN_ECOSYSTEM_PROC_DATASET = Namespace(
    "http://linked.data.gov.au/dataset/tern-ecosystem-processes/"
)

BDBSA = Namespace("http://linked.data.gov.au/def/bdbsa/")
BDBSA_DATASET = Namespace("http://linked.data.gov.au/dataset/bdbsa/")

THREE_PARKS_SAVANNA = Namespace("http://linked.data.gov.au/def/three-parks-savanna/")
THREE_PARKS_SAVANNA_DATASET = Namespace("http://linked.data.gov.au/dataset/three-parks-savanna/")

NSW_FMIP = Namespace("http://linked.data.gov.au/def/nsw-fmip/")
NSW_FMIP_DATASET = Namespace("http://linked.data.gov.au/dataset/nsw-fmip/")


class TernRdf:
    """This class provides an RDFLib Graph object with TERN-specific namespaces and prefix bindings."""

    # Prefix mappings to RDFLib namespaces. [COMMONS]
    common_prefixes = {
        "tern": TERN,
        "tern-sd": TERN_SD,
        "tern-ssn": TERN_SSN,
        "tern-loc": TERN_LOC,
        "tern-cv": TERN_CV,
        "biod": BIODIVERSITY,
        "bioreg": BIOREGION,
        "data": DATA,
        "dc": DC,
        "dcat": DCAT,
        "dct": DCTERMS,
        "dwct": DWCT,
        "epsg-crs": EPSG,
        "geo": W3CGEO,
        "geosparql": GEOSPARQL,
        "geosparql-ext": GEOSPARQL_EXT,
        "sf": SF,
        "ldp": LDP,
        "locn": LOCN,
        "owl": OWL,
        "plot": PLOT,
        "plot-x": PLOTX,
        "prov": PROV,
        "rdfs": RDFS,
        "reg": REG,
        "schema": SCHEMA,
        "skos": SKOS,
        "sosa": SOSA,
        "ssn-ext": SSN_EXT,
        "ssn": SSN,
        "time": TIME,
        "ui": UI,
        "unit": UNIT,
        "void": VOID,
    }

    # Prefix mappings to RDFLib namespaces. [CORVEG] // QBEIS
    corveg_prefixes = {"corveg": CORVEG_DATASET, "corveg-def": CORVEG, "corveg-cv": CORVEG_CV}

    # Prefix mappings to RDFLib namespaces. [AUSPLOTS] // Tern Surveillance Monitoring
    ausplots_prefixes = {
        "ausplots": AUSPLOTS_DATASET,
        "ausplots-def": AUSPLOTS,
        "ausplots-cv": AUSPLOTS_CV,
    }

    # Prefix mappings to RDFLib namespaces. [AUSPLOTS-FOREST]
    ausplots_forests_prefixes = {
        "ausplots-forest": AUSPLOTS_FOREST_DATASET,
        "ausplots-forest-def": AUSPLOTS_FOREST,
        "ausplots-cv": AUSPLOTS_CV,
    }

    # Prefix mappings to RDFLib namespaces. [WET_TROPICS_VERT]
    wet_tropics_vertebrate_prefixes = {
        "wet-tropics-vert": WET_TROPICS_VERT_DATASET,
        "wet-tropics-vert-def": WET_TROPICS_VERT,
    }

    tern_ecosystem_proc_prefixes = {
        "tern-ecosystem-processes": TERN_ECOSYSTEM_PROC_DATASET,
        "tern-ecosystem-processes-def": TERN_ECOSYSTEM_PROC,
    }

    bdbsa_prefixes = {
        "bdbsa": BDBSA_DATASET,
        "bdbsa-def": BDBSA,
    }

    three_parks_savanna_prefixes = {
        "three-parks-savanna": THREE_PARKS_SAVANNA_DATASET,
        "three-parks-savanna-def": THREE_PARKS_SAVANNA,
    }

    nsw_fmip_prefixes = {
        "nsw-fmip": NSW_FMIP_DATASET,
        "nsw-fmip-def": NSW_FMIP,
    }

    # Format mappings for RDFLib serialisation options.
    formats = {"turtle": ".ttl", "xml": ".rdf", "nt": ".nt", "nquads": ".nq", "trig": ".trig"}

    @staticmethod
    def Graph(projects=None):
        """
        Return an RDFLib Graph object with bindings to TERN-specific namespaces and nice prefixes.
        :param projects: String list of Projects whose prefixes will be added to the Graph
        :return RDFLib Graph object
        """
        if projects is None:
            projects = []

        g = Graph()

        for prefix in TernRdf.common_prefixes:
            g.bind(prefix, str(TernRdf.common_prefixes[prefix]))

        # If no projects are defined, bind all prefixes to be sure it doesn't break any old program using this library
        if len(projects) == 0:
            for prefix in TernRdf.ausplots_prefixes:
                g.bind(prefix, str(TernRdf.ausplots_prefixes[prefix]))

            for prefix in TernRdf.corveg_prefixes:
                g.bind(prefix, str(TernRdf.corveg_prefixes[prefix]))

            for prefix in TernRdf.ausplots_forests_prefixes:
                g.bind(prefix, str(TernRdf.ausplots_forests_prefixes[prefix]))
        else:
            if AUSPLOTS_BINDINGS in projects:
                for prefix in TernRdf.ausplots_prefixes:
                    g.bind(prefix, str(TernRdf.ausplots_prefixes[prefix]))

            if CORVEG_BINDINGS in projects:
                for prefix in TernRdf.corveg_prefixes:
                    g.bind(prefix, str(TernRdf.corveg_prefixes[prefix]))

            if AUSPLOTS_FOREST_BINDINGS in projects:
                for prefix in TernRdf.ausplots_forests_prefixes:
                    g.bind(prefix, str(TernRdf.ausplots_forests_prefixes[prefix]))

            if WET_TROPICS_VERT_BINDINGS in projects:
                for prefix in TernRdf.wet_tropics_vertebrate_prefixes:
                    g.bind(prefix, str(TernRdf.wet_tropics_vertebrate_prefixes[prefix]))

            if TERN_ECOSYSTEM_PROC_BINDINGS in projects:
                for prefix in TernRdf.tern_ecosystem_proc_prefixes:
                    g.bind(prefix, str(TernRdf.tern_ecosystem_proc_prefixes[prefix]))

            if BDBSA_BINDINGS in projects:
                for prefix in TernRdf.bdbsa_prefixes:
                    g.bind(prefix, str(TernRdf.bdbsa_prefixes[prefix]))

            if THREE_PARKS_SAVANNA_BINDINGS in projects:
                for prefix in TernRdf.three_parks_savanna_prefixes:
                    g.bind(prefix, str(TernRdf.three_parks_savanna_prefixes[prefix]))

            if NSW_FMIP_BINDINGS in projects:
                for prefix in TernRdf.nsw_fmip_prefixes:
                    g.bind(prefix, str(TernRdf.nsw_fmip_prefixes[prefix]))

        return g

    @staticmethod
    def Dataset(projects=None):

        if projects is None:
            projects = []

        parent_graph = Dataset()

        for prefix in TernRdf.common_prefixes:
            parent_graph.bind(prefix, str(TernRdf.common_prefixes[prefix]))

        # If no projects are defined, bind all prefixes to be sure it doesn't break any old program using this library
        if len(projects) == 0:
            for prefix in TernRdf.ausplots_prefixes:
                parent_graph.bind(prefix, str(TernRdf.ausplots_prefixes[prefix]))

            for prefix in TernRdf.corveg_prefixes:
                parent_graph.bind(prefix, str(TernRdf.corveg_prefixes[prefix]))

            for prefix in TernRdf.ausplots_forests_prefixes:
                parent_graph.bind(prefix, str(TernRdf.ausplots_forests_prefixes[prefix]))
        else:
            if AUSPLOTS_BINDINGS in projects:
                for prefix in TernRdf.ausplots_prefixes:
                    parent_graph.bind(prefix, str(TernRdf.ausplots_prefixes[prefix]))

            if CORVEG_BINDINGS in projects:
                for prefix in TernRdf.corveg_prefixes:
                    parent_graph.bind(prefix, str(TernRdf.corveg_prefixes[prefix]))

            if AUSPLOTS_FOREST_BINDINGS in projects:
                for prefix in TernRdf.ausplots_forests_prefixes:
                    parent_graph.bind(prefix, str(TernRdf.ausplots_forests_prefixes[prefix]))

            if WET_TROPICS_VERT_BINDINGS in projects:
                for prefix in TernRdf.wet_tropics_vertebrate_prefixes:
                    parent_graph.bind(prefix, str(TernRdf.wet_tropics_vertebrate_prefixes[prefix]))

            if TERN_ECOSYSTEM_PROC_BINDINGS in projects:
                for prefix in TernRdf.tern_ecosystem_proc_prefixes:
                    parent_graph.bind(prefix, str(TernRdf.tern_ecosystem_proc_prefixes[prefix]))

            if BDBSA_BINDINGS in projects:
                for prefix in TernRdf.bdbsa_prefixes:
                    parent_graph.bind(prefix, str(TernRdf.bdbsa_prefixes[prefix]))

            if THREE_PARKS_SAVANNA_BINDINGS in projects:
                for prefix in TernRdf.three_parks_savanna_prefixes:
                    parent_graph.bind(prefix, str(TernRdf.three_parks_savanna_prefixes[prefix]))

            if NSW_FMIP_BINDINGS in projects:
                for prefix in TernRdf.nsw_fmip_prefixes:
                    parent_graph.bind(prefix, str(TernRdf.nsw_fmip_prefixes[prefix]))

        return parent_graph

    @staticmethod
    def get_file_extension(a_format):
        """

        :param a_format:
        :return: one of the formats listed in formats dict - see above
        """
        return TernRdf.formats[a_format]
