import logging
import time


from tern_rdf.utils import create_session


def sparql(sparql_endpoint: str, sparql_query: str) -> dict:
    """Make a SPARQL 1.1 query to a SPARQL endpoint.

    Returns a Python dict of the SPARQL JSON response.

    Use this for convenience, otherwise for more functionality (such as auth), use SPARQLWrapper.
    """

    logger = logging.getLogger(__name__)

    headers = {
        'content-type': 'application/sparql-query',
        'accept': 'application/sparql-results+json'
    }

    logger.info('Sending SPARQL query')
    starttime = time.time()

    http = create_session()
    r = http.post('{0}'.format(sparql_endpoint), data=sparql_query, headers=headers)
    logger.info('Response received. Status: {}'.format(r.status_code))
    if r.status_code == 200:
        logger.info('Response document size: {}'.format(len(r.json()['results']['bindings'])))
    logger.info('Response took {:.2f} seconds'.format(time.time() - starttime))

    assert r.status_code >= 200 and r.status_code < 300, "Didn't get an expected status code. Message: {}".format(
        r.content.decode('utf-8'))
    data = r.json()
    if not data.get('head') or not data.get('results'):
        raise Exception('Error, faulty sparql response.')

    return data


def assign_sparql_result_row_value(row: dict, key: str):
    """Return the 'column' value of the row if it exists, else return an empty list.

    :param row: A SPARQL JSON result row.
    :param key: Name of the SPARQL JSON result row's column.
    """
    return [row.get(key).get('value')] if row.get(key) else list()


def assign_single_sparql_result_row_value(row: dict, key: str):
    """Return the 'column' value of the row if it exists, else return None

    :param row: A SPARQL JSON result row.
    :param key: Name of the SPARQL JSON result row's column.
    """
    return row.get(key).get('value') if row.get(key) else None


def process_sparql_result(result: dict, ignore_keys: list=[], key_value_not_a_list: list =[]) -> dict:
    """Return a processed dict of the SPARQL query result.

    This function processes the SPARQL query result set and converts each variable values into a list.

    Duplicated rows are "squashed" and appended to the list.

    If key values are singular values and not a list of values, add the key to the key_value_not_a_list argument.
    If the result dictionary contains multiple URIs with different values of the same key, the latest key value will
    be overriden.

    E.g.
    >>> label1 = 'label1'
    >>> label2 = 'label2'
    >>> label = ['label1', 'label2']

    :param result: SPARQL result to be processed.
    :param ignore_keys: Keys to be ignored in the result set.
    :param key_value_not_a_list: Keys that have values that are not lists.
    """
    vars = result['head']['vars']
    assert 'uri' in vars, f'Missing SPARQL variable "uri". Instead, got {vars}'
    docs = {}

    for row in result['results']['bindings']:
        uri = row['uri']['value']
        if uri not in docs:
            doc = {}
            for var in vars:
                if var in ignore_keys:
                    # Skip key (we don't want it in the returned doc).
                    continue
                if var == 'uri':
                    doc['uri'] = row['uri']['value']
                else:
                    if var not in key_value_not_a_list:
                        doc[var] = assign_sparql_result_row_value(row, var)
                    else:
                        doc[var] = assign_single_sparql_result_row_value(row, var)
            docs[uri] = doc
        else:
            for var in vars:
                if var in ignore_keys:
                    # Skip key (we don't want it in the returned doc).
                    continue
                # If var already exists in doc
                # and there is a value in the current row
                # and the value is not currently in the list, then proceed.
                try:
                    if var not in key_value_not_a_list:
                        if var in docs[uri] and row.get(var).get('value') and row[var]['value'] not in docs[uri][var]:
                            docs[uri][var].append(row[var]['value'])
                    else:
                        doc[var] = assign_single_sparql_result_row_value(row, var)
                except:
                    pass
    return docs
