import logging
import time
from datetime import datetime

from elasticsearch import __version__ as __es_version__

from .sparql import sparql

if __es_version__[0] == 7:
    # compatibility wrappers:
    from .es7 import Elasticsearch, helpers
elif __es_version__[0] == 8:
    from .es8 import Elasticsearch, helpers
else:
    raise ImportError("Elasticsearch client version not supported: {}".format(__es_version__))


# TODO:
#    check ES client version, and adjust function calls accordingly.
#    support: 7 and 8
def get_es_connection(es_hosts: str or list, es_user: str, es_pass: str) -> Elasticsearch:
    logger = logging.getLogger(__name__)
    # Build and test connection to Elasticsearch
    es = Elasticsearch(
        hosts=es_hosts,
        http_auth=(es_user, es_pass),
        timeout=60,
        # Don't verify certs. In k8s, we connect directly the Elasticsearch via its internal DNS name without SSL.
        verify_certs=False,
    )
    try:
        es.info()
    except Exception as e:
        logger.error("Can't connect to elasticsearch: %s", e)
        raise
    return es


def update_index(
    es: Elasticsearch,
    alias: str,
    index_name: str,
    index_actions: set,
    additional_aliases: list = None,
):
    """Ingest all index_actions into index named index_name.

    After successful indexing update alias to include new index and delete all
    previously existing indices on alias.
    """
    logger = logging.getLogger(__name__)
    starttime = time.time()
    try:
        # 1. get existing indices
        # collect alias update actions
        update_actions = []
        for idx in es.indices.get(index=f"{alias}*").keys():
            # remove currently existing indices
            if idx != index_name:
                # ensure we don't remove new index
                update_actions.append({"remove_index": {"index": idx}})

        logger.info("Ingesting documents into Elasticsearch {}".format(index_name))
        helpers.bulk(es, index_actions)
        logger.info("Switch alias to new index")

        # add alias to new index
        update_actions.append({"add": {"index": index_name, "alias": alias}})
        if additional_aliases:
            update_actions.append({"add": {"index": index_name, "aliases": additional_aliases}})
        # update index aliases
        es.indices.update_aliases(body={"actions": update_actions})

        logger.info("Completed indexing in {:.2f} seconds.".format(time.time() - starttime))
    except Exception as e:
        logger.error("Indexing failed: %s", e)
        # TODO: depending on failure we might want to clean up es indices.
        raise


def index(
    sparql_endpoint: str,
    sparql_query: str,
    prep_index_actions_func,
    index_alias: str,
    es_hosts: str or list,
    es_user: str,
    es_pass: str,
    mapping: dict = None,
    additional_aliases: list = None,
    **kwargs,
) -> None:
    """Index the results of a SPARQL query into Elasticsearch.

    Additional kwargs passed in will be made available to the function assigned to prep_index_actions_func.
    """
    data = sparql(sparql_endpoint, sparql_query)

    index_name = "{0}-{1}".format(index_alias, datetime.utcnow().strftime("%Y-%m-%dt%H-%M-%S"))

    index_actions = prep_index_actions_func(index_name, data, **kwargs)

    es = get_es_connection(es_hosts, es_user, es_pass)

    es.indices.create(index=index_name, body=mapping)

    update_index(es, index_alias, index_name, index_actions, additional_aliases)
