import socket

import requests
from requests.adapters import HTTPAdapter


class HTTPAdapterSocketOptions(HTTPAdapter):
    """HTTP Adapter to enable socket options"""
    def __init__(self, *args, **kwargs):
        self.socket_options = kwargs.pop('socket_options', None)
        self.timeout = kwargs.pop('timeout', 60 * 10)  # default 10 minutes
        super(HTTPAdapterSocketOptions, self).__init__(*args, **kwargs)

    def init_poolmanager(self, *args, **kwargs):
        if self.socket_options is not None:
            kwargs['socket_options'] = self.socket_options
        super(HTTPAdapterSocketOptions, self).init_poolmanager(*args, **kwargs)

    def send(self, request, **kwargs):
        # Set a timeout on all requests.
        timeout = kwargs.get('timeout')
        if timeout is None:
            kwargs['timeout'] = self.timeout
        return super().send(request, **kwargs)


def create_session() -> requests.Session:
    http = requests.Session()

    # Only set socket options if TCP_KEEPIDLE, TCP_KEEPINTVL and TCP_KEEPCNT is available.
    if hasattr(socket, "TCP_KEEPIDLE") and hasattr(socket, "TCP_KEEPINTVL") and hasattr(socket, "TCP_KEEPCNT"):
        socket_options = [
            (socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1),
            (socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 60),
            (socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 30),
            (socket.IPPROTO_TCP, socket.TCP_KEEPCNT, 10),
        ]
    else:
        socket_options = list()

    adapter = HTTPAdapterSocketOptions(socket_options=socket_options, max_retries=3)
    # Clear existing adapters set by the requests library and set our own with prefix 'http'.
    http.adapters.clear()
    http.mount('http', adapter)

    return http
