"""Compat wrappers to use ES 8 client."""

from elasticsearch import Elasticsearch as Elasticsearch8  # noqa


class IndicesClient(Elasticsearch8):
    """Es7 compat interface."""

    # Es7 signature would be (body, params=None, headers=None)
    #     Es8 no longer support headers
    #     everything else could be passed as **body, **params to ES8 client
    def update_aliases(self, body):
        """Update the elasticsearch aliases.

        Args:
            body: the request body. Defaults to None

        Returns:
            The update result
        """
        return super().indices.update_aliases(**body)

    # ES7 signature: index, body=None, params=None, headers=None
    #     Es8 no longer supports headers
    #     everything else could be passed as index=index, **body, **params,
    def create(self, index, body=None):
        """Create an Elasticsaerch index.

        Args:
            index: the index name
            body: the request body. Defaults to None

        Returns:
            _type_: _description_
        """
        if body is None:
            body = {}
        return super().indices.create(index=index, **body)


class Elasticsearch(Elasticsearch8):
    """Es7 compat client interface."""

    def __init__(self, *args, **kwargs):
        """Initialise.

        Args:
            args: additional arguments
            kwargs: Additional keyword arguments.
        """
        # update deprecated parameters:
        if "http_auth" in kwargs:
            kwargs["basic_auth"] = kwargs["http_auth"]
            del kwargs["basic_auth"]
        if "timout" in kwargs:
            kwargs["request_timeout"] = kwargs["timeout"]
            del kwargs["timeout"]
        super().__init__(*args, **kwargs)
        # patch APIs we use
        self.indices = IndicesClient(self)
