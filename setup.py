import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt") as req:
    install_requires = req.read().split("\n")

setuptools.setup(
    name="TERN RDF",
    version="0.0.49",
    author="TERN Australia",
    author_email="esupport@tern.org.au",
    description="A Python library to work with RDF at TERN.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/terndatateam/tern_rdf/src",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=install_requires,
)
