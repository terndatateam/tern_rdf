# TERN RDF
TERN RDF is a Python package containing the namespaces and prefixes for all RDF resources managed by TERN.

Since the number of datasets and projects is increasing, it is possible to preload the Graph with the namespaces and 
prefixes of one or more specific projects as AUSPLOTS or CORVEG, avoiding to bind the unnecessary ones.   

Current version contains:

- Common **TERN** namespaces and prefixes in all projects (always preloaded).
- **CORVEG** namespaces and prefixes.
- **AUSPLOTS** namespaces and prefixes.
- **AUSPLOTS-FOREST** namespaces and prefixes.
- **WILLIAMS_WET_TROPICS_VERTEBRATES_DATASET** namespaces and prefixes.
- **TERN_ECOSYSTEM_PROCESSES** namespaces and prefixes.
- **BDBSA** namespaces and prefixes.
- **THREE_PARKS_SAVANNA** namespaces and prefixes.
- **NSW-FMIP** namespaces and prefixes.

## Installation
Install using **pip**
```
pip install --upgrade git+https://bitbucket.org/terndatateam/tern_rdf.git@master
```

Using the _--upgrade_ option we insure that any small change or fix in the package that was not resulting in a new version is available.

## Usage
```python
if __name__ == '__main__':
    # Import the TERN RDF package.
    from tern_rdf import TernRdf, AUSPLOTS_BINDINGS
    
    # Create an RDFLib.Graph object preloaded with TERN-specific and Ausplots RDF namespaces and prefixes.
    g = TernRdf.Graph([AUSPLOTS_BINDINGS, ])

    # Do things...
```

The package is backward compatible with the previous version, so it is not mandatory to indicate the project. By default, all projects will be preloaded:
```python
if __name__ == '__main__':
    # Import the TERN RDF package.
    from tern_rdf import TernRdf
    
    # Create an RDFLib.Graph object preloaded with TERN-specific and all available projects RDF namespaces and prefixes.
    g = TernRdf.Graph()

    # Do things...
```

## Tests
```bash
python -m pytest
```

## Contact
**Javier Sanchez Gonzalez**  
*Senior Software Engineer*  
[j.sanchezgonzalez@uq.edu.au](mailto:j.sanchezgonzalez@uq.edu.au)  


**Mosheh EliYahu**  
*Software Engineer*  
[mosheh.eliyahu@adelaide.edu.au](mailto:mosheh.eliyahu@adelaide.edu.au)  

